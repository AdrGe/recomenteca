import 'package:basesita/nva_peli.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:moor_flutter/moor_flutter.dart' hide Column;

import 'db.dart';
import 'package:share/share.dart';

class DBservice extends ChangeNotifier {
  DBservice(this._base);
  final AppDatabase _base;

  void nvaPeli(MovieEntityData peli) async {
    await _base
        .insertMovie(MovieEntityCompanion(
            name: Value(peli.name),
            tipo: Value(peli.tipo),
            visto: Value(peli.visto),
            nota: Value(peli.nota),
            recomen: Value(peli.recomen)))
        .then((value) => {
              print(peli),
              print(value),
            })
        .catchError((error) => {
              print(error),
            });
  }

  void peliVista(MovieEntityData peli) async {
    MovieEntityData nvaPeli = peli.copyWith(visto: true);
    await _base.updateMovie(nvaPeli);
    notifyListeners();
  }

  void borraPeli(MovieEntityData peli) async {
    await _base.deleteMovie(peli);
    notifyListeners();
  }

  editaPeli(MovieEntityData peli, BuildContext context) {
    // TODO: cargar la peli y mostrala en nva_peli
    Navigator.of(context)
        .push(MaterialPageRoute(builder: (context) => NuevaPeli(peli)));
  }

  void compartePeli(MovieEntityData peli) {
    Share.share('Te comparto-> ${peli.tipo} : ${peli.name}, ${peli.nota}',
        subject: 'Recomiendo');
  }

  /*  final StreamProvider pelis = appDatabase.lasPelis();
  final StreamProvider juegos = appDatabase.losJuegos();
  final StreamProvider libros = appDatabase.losLibros();
  final StreamProvider series = appDatabase.lasSeries(); */

}
