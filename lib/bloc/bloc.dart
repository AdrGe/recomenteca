// import 'package:basesita/nva_peli.dart';
import 'package:basesita/data/daos.dart';
import 'package:flutter/material.dart';
import 'package:moor_flutter/moor_flutter.dart' hide Column;

import 'package:basesita/data/db.dart';
import 'package:provider/provider.dart';
import 'package:share/share.dart';

class recBloc {
  final _dao = MoviesDao(Appdatabase); // Provider.of<MoviesDao>(MoviesDao());

  void nvaPeli(MovieEntry peli) async {
    await _dao
        .insertMovie(MoviesCompanion(
            name: Value(peli.name),
            tipo: Value(peli.tipo),
            visto: Value(peli.visto),
            nota: Value(peli.nota),
            recomen: Value(peli.recomen)))
        .then(
          (value) => print(value),
        )
        .catchError(
          (error) => print(error),
        );
  }

  void peliVista(MovieEntry peli) async {
    MovieEntry nvaPeli = peli.copyWith(visto: true);
    await _dao.updateMovie(nvaPeli);
    // notifyListeners();
  }

  void borraPeli(MovieEntry peli) async {
    await _dao.deleteMovie(peli);
    // notifyListeners();
  }

  editaPeli(MovieEntry peli, BuildContext context) {
    // TODO: cargar la peli y mostrala en nva_peli
/*     Navigator.of(context)
        .push(MaterialPageRoute(builder: (context) => NuevaPeli(peli))); */
  }

  /*  final StreamProvider pelis = appDatabase.lasPelis();
  final StreamProvider juegos = appDatabase.losJuegos();
  final StreamProvider libros = appDatabase.losLibros();
  final StreamProvider series = appDatabase.lasSeries(); */

}
