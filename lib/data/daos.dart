import 'tablas.dart';
import 'package:moor_flutter/moor_flutter.dart';
import 'package:moor/moor.dart';
import 'db.dart';

@UseDao(tables: [Movies])
class MoviesDao extends DatabaseAccessor<AppDatabase> {
  final AppDatabase db;
  MoviesDao(this.db) : super(db);

/* Future insertTask(Insertable<Task> task) => into(tasks).insert(task);
Future updateTask(Insertable<Task> task) => update(tasks).replace(task);
Future deleteTask(Insertable<Task> task) => delete(tasks).delete(task); */

  Future<int> insertMovie(MoviesCompanion entry) {
    return into($MoviesTable(db)).insert(entry);
  }

  Future updateMovie(Insertable<MovieEntry> entry) {
    return update($MoviesTable(db)).replace(entry);
  }

  // Borra un registro
  Future deleteMovie(MovieEntry movie) {
    return delete($MoviesTable(db)).delete(movie);
  }
  //Borra la tabla completa
  //  Future<int> deleteTable() => delete(MovieEntry movie).go();

  //trae todo, pero se cuida de que filtro eté vacío
  Stream<List<MovieEntry>> watchAll(String filtro) {
    if (filtro == '') {
      return (select($MoviesTable(db))
            ..orderBy(
              ([
                (t) => OrderingTerm(expression: t.name),
              ]),
            ))
          .watch();
    }
    return (select($MoviesTable(db))
          ..orderBy(
            ([
              (t) => OrderingTerm(expression: t.name),
            ]),
          )
          ..where((t) => t.tipo.equals(filtro)))
        .watch();
  }

// Trae solo pendientes de ver
  Stream<List<MovieEntry>> watchPendiente(String filtro) {
    if (filtro == '') {
      return (select($MoviesTable(db))
            ..orderBy(
              ([
                (t) => OrderingTerm(expression: t.name),
              ]),
            )
            ..where((t) => t.visto.equals(false)))
          .watch();
    }
    return (select($MoviesTable(db))
          ..orderBy(
            ([
              (t) => OrderingTerm(expression: t.name),
            ]),
          )
          ..where((t) => t.visto.equals(false) & t.tipo.equals(filtro)))
        .watch();
  }

// Trae vistos
  Stream<List<MovieEntry>> watchVistos(String filtro) {
    if (filtro == '') {
      return (select($MoviesTable(db))
            ..orderBy(
              ([
                (t) => OrderingTerm(expression: t.name),
              ]),
            ))
          .watch();
    }
    return (select($MoviesTable(db))
          ..orderBy(
            ([
              (t) => OrderingTerm(expression: t.name),
            ]),
          )
          ..where((t) => t.visto.equals(true) & t.tipo.equals(filtro)))
        .watch();
  }
}
