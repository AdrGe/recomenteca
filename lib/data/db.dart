import 'dart:io';

import 'package:basesita/data/daos.dart';
import 'package:basesita/data/tablas.dart';
import 'package:path_provider/path_provider.dart';
import 'package:path/path.dart' as p;
import 'package:moor/moor.dart';
import 'package:moor_ffi/moor_ffi.dart';

part 'db.g.dart';

/* @DataClassName('MovieEntry')
class Movies extends Table {
  IntColumn get id => integer().autoIncrement()();
  TextColumn get name => text().withLength(min: 1, max: 100)();
  TextColumn get tipo => text().withLength(min: 5, max: 10)();
  TextColumn get nota => text().nullable()();
  TextColumn get recomen => text().withLength(min: 1, max: 20).nullable()();
  BoolColumn get visto => boolean().withDefault(Constant(false))();
} */

LazyDatabase _openConnection() {
  // the LazyDatabase util lets us find the right location for the file async.
  return LazyDatabase(() async {
    // put the database file, called db.sqlite here, into the documents folder
    // for your app.
    final dbFolder = await getApplicationDocumentsDirectory();
    final file = File(p.join(dbFolder.path, 'db.sqlite'));
    return VmDatabase(file);
  });
}

@UseMoor(tables: [Movies], daos: [MoviesDao])
class AppDatabase extends _$AppDatabase {
// por este ^ nombre la vamos a referenciar en la app
  AppDatabase() : super(_openConnection());

  @override
  // esto sirve para ejecutar migraciones si cambia el schema
  int get schemaVersion => 1;

  /* @override
  MigrationStrategy get migration {
    return MigrationStrategy(onCreate: (Migrator m) {
      return m.createAllTables();
    },
        // onUpgrade: (Migrator m, int from, int to) async {
        //   if (from == 1) {
        //     await m.addColumn(MovieEntityData, alguito);
        //   }
        // },
        beforeOpen: (details) async {
      if (details.wasCreated) {
        await into(Movies).insert(
          MoviesCompanion(
            name: Value('El dependiente'),
            tipo: Value('pelicula'),
            recomen: Value('La App'),
            nota: Value('de Leonardo Favio. Clásico de todos los tiempos'),
            visto: Value(false),
          ),
        );
        await into(movie).insert(
          MovieCompanion(
            name: Value('Rayuela'),
            tipo: Value('libro'),
            recomen: Value('La App'),
            nota: Value('Julio Cortázar. Clásico del boom latinoamericano'),
            visto: Value(false),
          ),
        );
        await into(movie).insert(
          MovieCompanion(
            name: Value('Breaking Bad'),
            tipo: Value('serie'),
            recomen: Value('La App'),
            nota: Value('Mejor serie de todos los tiempos'),
            visto: Value(false),
          ),
        );
        await into(movie).insert(
          MovieCompanion(
            name: Value('Shadow of the colosus'),
            tipo: Value('Juego'),
            recomen: Value('La App'),
            nota: Value(
                'Juego producido por SONY para la PS2, que sigue siendo de los mejores del mercado.'),
            visto: Value(false),
          ),
        );
      }
    });
  }*/
} //fin de la clase

/*@UseDao(tables: [Movies])
class MoviesDao extends DatabaseAccessor<AppDatabase> {
  final AppDatabase db;

  // Called by the AppDatabase class
  MoviesDao(this.db) : super(db);

//trae todo sin orden ni concierto(?)
  Stream<List<MovieEntry>> watchAll() => select($MoviesTable(db)).watch();

  Future updateMovie(MovieEntry entry) {
    return update($MoviesTable(db)).replace(entry);
  }

  Future<int> insertMovie(MoviesCompanion entry) {
    return into($MoviesTable(db)).insert(entry);
  }

  // Borra un registro
  Future deleteMovie(MovieEntry movie) {
    return delete($MoviesTable(db)).delete(movie);
  }
    //Borra la tabla completa
//  Future<int> deleteTable() => delete(MovieEntry movie).go();

  Stream<List<MovieEntry>> losLibros() {
    return (select($MoviesTable(db))
          ..orderBy(
            ([
              (l) => OrderingTerm(expression: l.name),
            ]),
          )
          ..where((l) => l.tipo.equals('libro')))
        .watch();
  }

  Stream<List<MovieEntry>> losJuegos() {
    return (select($MoviesTable(db))
          ..orderBy(
            ([
              (j) => OrderingTerm(expression: j.name),
            ]),
          )
          ..where((j) => j.tipo.equals('juego')))
        .watch();
  }

  Stream<List<MovieEntry>> lasPelis() {
    return (select($MoviesTable(db))
          ..orderBy(
            ([
              (p) => OrderingTerm(expression: p.name),
            ]),
          )
          ..where((p) => p.tipo.equals('pelicula')))
        .watch();
  }

  Stream<List<MovieEntry>> lasSeries() {
    return (select($MoviesTable(db))
          ..orderBy(
            ([
              (s) => OrderingTerm(expression: s.name),
            ]),
          )
          ..where((s) => s.tipo.equals('serie')))
        .watch();
  }
  //Ejemplo de query complejo
  Stream<List<Movies>> watchAllMovies() {
    // Wrap the whole select statement in parenthesis
    return (select(movies)
          // Statements like orderBy and where return void, so, they need to use a cascading ".." operator
          ..orderBy(
            ([
              // alfabetico
              (m) => OrderingTerm(expression: m.name),
              // se pueden agregar mas expresiones: (t) => OrderingTerm(expression: t.dni),
            ]),
          )
          // también filtrar por algún campo
          ..where((m) => m.visto.equals(false))),
           )
        // watch the whole select statement
        .watch();
  }
}*/
