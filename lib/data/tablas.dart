import 'package:moor_flutter/moor_flutter.dart';
import 'package:moor/moor.dart';

@DataClassName('MovieEntry')
class Movies extends Table {
  IntColumn get id => integer().autoIncrement()();
  TextColumn get name => text().withLength(min: 1, max: 100)();
  TextColumn get tipo => text().withLength(min: 5, max: 10)();
  TextColumn get nota => text().nullable()();
  TextColumn get recomen => text().withLength(min: 1, max: 20).nullable()();
  BoolColumn get visto => boolean().withDefault(Constant(false))();
}
