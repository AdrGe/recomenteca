// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'db.dart';

// **************************************************************************
// MoorGenerator
// **************************************************************************

// ignore_for_file: unnecessary_brace_in_string_interps, unnecessary_this
class MovieEntry extends DataClass implements Insertable<MovieEntry> {
  final int id;
  final String name;
  final String tipo;
  final String nota;
  final String recomen;
  final bool visto;
  MovieEntry(
      {@required this.id,
      @required this.name,
      @required this.tipo,
      this.nota,
      this.recomen,
      @required this.visto});
  factory MovieEntry.fromData(Map<String, dynamic> data, GeneratedDatabase db,
      {String prefix}) {
    final effectivePrefix = prefix ?? '';
    final intType = db.typeSystem.forDartType<int>();
    final stringType = db.typeSystem.forDartType<String>();
    final boolType = db.typeSystem.forDartType<bool>();
    return MovieEntry(
      id: intType.mapFromDatabaseResponse(data['${effectivePrefix}id']),
      name: stringType.mapFromDatabaseResponse(data['${effectivePrefix}name']),
      tipo: stringType.mapFromDatabaseResponse(data['${effectivePrefix}tipo']),
      nota: stringType.mapFromDatabaseResponse(data['${effectivePrefix}nota']),
      recomen:
          stringType.mapFromDatabaseResponse(data['${effectivePrefix}recomen']),
      visto: boolType.mapFromDatabaseResponse(data['${effectivePrefix}visto']),
    );
  }
  factory MovieEntry.fromJson(Map<String, dynamic> json,
      {ValueSerializer serializer = const ValueSerializer.defaults()}) {
    return MovieEntry(
      id: serializer.fromJson<int>(json['id']),
      name: serializer.fromJson<String>(json['name']),
      tipo: serializer.fromJson<String>(json['tipo']),
      nota: serializer.fromJson<String>(json['nota']),
      recomen: serializer.fromJson<String>(json['recomen']),
      visto: serializer.fromJson<bool>(json['visto']),
    );
  }
  @override
  Map<String, dynamic> toJson(
      {ValueSerializer serializer = const ValueSerializer.defaults()}) {
    return <String, dynamic>{
      'id': serializer.toJson<int>(id),
      'name': serializer.toJson<String>(name),
      'tipo': serializer.toJson<String>(tipo),
      'nota': serializer.toJson<String>(nota),
      'recomen': serializer.toJson<String>(recomen),
      'visto': serializer.toJson<bool>(visto),
    };
  }

  @override
  MoviesCompanion createCompanion(bool nullToAbsent) {
    return MoviesCompanion(
      id: id == null && nullToAbsent ? const Value.absent() : Value(id),
      name: name == null && nullToAbsent ? const Value.absent() : Value(name),
      tipo: tipo == null && nullToAbsent ? const Value.absent() : Value(tipo),
      nota: nota == null && nullToAbsent ? const Value.absent() : Value(nota),
      recomen: recomen == null && nullToAbsent
          ? const Value.absent()
          : Value(recomen),
      visto:
          visto == null && nullToAbsent ? const Value.absent() : Value(visto),
    );
  }

  MovieEntry copyWith(
          {int id,
          String name,
          String tipo,
          String nota,
          String recomen,
          bool visto}) =>
      MovieEntry(
        id: id ?? this.id,
        name: name ?? this.name,
        tipo: tipo ?? this.tipo,
        nota: nota ?? this.nota,
        recomen: recomen ?? this.recomen,
        visto: visto ?? this.visto,
      );
  @override
  String toString() {
    return (StringBuffer('MovieEntry(')
          ..write('id: $id, ')
          ..write('name: $name, ')
          ..write('tipo: $tipo, ')
          ..write('nota: $nota, ')
          ..write('recomen: $recomen, ')
          ..write('visto: $visto')
          ..write(')'))
        .toString();
  }

  @override
  int get hashCode => $mrjf($mrjc(
      id.hashCode,
      $mrjc(
          name.hashCode,
          $mrjc(tipo.hashCode,
              $mrjc(nota.hashCode, $mrjc(recomen.hashCode, visto.hashCode))))));
  @override
  bool operator ==(dynamic other) =>
      identical(this, other) ||
      (other is MovieEntry &&
          other.id == this.id &&
          other.name == this.name &&
          other.tipo == this.tipo &&
          other.nota == this.nota &&
          other.recomen == this.recomen &&
          other.visto == this.visto);
}

class MoviesCompanion extends UpdateCompanion<MovieEntry> {
  final Value<int> id;
  final Value<String> name;
  final Value<String> tipo;
  final Value<String> nota;
  final Value<String> recomen;
  final Value<bool> visto;
  const MoviesCompanion({
    this.id = const Value.absent(),
    this.name = const Value.absent(),
    this.tipo = const Value.absent(),
    this.nota = const Value.absent(),
    this.recomen = const Value.absent(),
    this.visto = const Value.absent(),
  });
  MoviesCompanion.insert({
    this.id = const Value.absent(),
    @required String name,
    @required String tipo,
    this.nota = const Value.absent(),
    this.recomen = const Value.absent(),
    this.visto = const Value.absent(),
  })  : name = Value(name),
        tipo = Value(tipo);
  MoviesCompanion copyWith(
      {Value<int> id,
      Value<String> name,
      Value<String> tipo,
      Value<String> nota,
      Value<String> recomen,
      Value<bool> visto}) {
    return MoviesCompanion(
      id: id ?? this.id,
      name: name ?? this.name,
      tipo: tipo ?? this.tipo,
      nota: nota ?? this.nota,
      recomen: recomen ?? this.recomen,
      visto: visto ?? this.visto,
    );
  }
}

class $MoviesTable extends Movies with TableInfo<$MoviesTable, MovieEntry> {
  final GeneratedDatabase _db;
  final String _alias;
  $MoviesTable(this._db, [this._alias]);
  final VerificationMeta _idMeta = const VerificationMeta('id');
  GeneratedIntColumn _id;
  @override
  GeneratedIntColumn get id => _id ??= _constructId();
  GeneratedIntColumn _constructId() {
    return GeneratedIntColumn('id', $tableName, false,
        hasAutoIncrement: true, declaredAsPrimaryKey: true);
  }

  final VerificationMeta _nameMeta = const VerificationMeta('name');
  GeneratedTextColumn _name;
  @override
  GeneratedTextColumn get name => _name ??= _constructName();
  GeneratedTextColumn _constructName() {
    return GeneratedTextColumn('name', $tableName, false,
        minTextLength: 1, maxTextLength: 100);
  }

  final VerificationMeta _tipoMeta = const VerificationMeta('tipo');
  GeneratedTextColumn _tipo;
  @override
  GeneratedTextColumn get tipo => _tipo ??= _constructTipo();
  GeneratedTextColumn _constructTipo() {
    return GeneratedTextColumn('tipo', $tableName, false,
        minTextLength: 5, maxTextLength: 10);
  }

  final VerificationMeta _notaMeta = const VerificationMeta('nota');
  GeneratedTextColumn _nota;
  @override
  GeneratedTextColumn get nota => _nota ??= _constructNota();
  GeneratedTextColumn _constructNota() {
    return GeneratedTextColumn(
      'nota',
      $tableName,
      true,
    );
  }

  final VerificationMeta _recomenMeta = const VerificationMeta('recomen');
  GeneratedTextColumn _recomen;
  @override
  GeneratedTextColumn get recomen => _recomen ??= _constructRecomen();
  GeneratedTextColumn _constructRecomen() {
    return GeneratedTextColumn('recomen', $tableName, true,
        minTextLength: 1, maxTextLength: 20);
  }

  final VerificationMeta _vistoMeta = const VerificationMeta('visto');
  GeneratedBoolColumn _visto;
  @override
  GeneratedBoolColumn get visto => _visto ??= _constructVisto();
  GeneratedBoolColumn _constructVisto() {
    return GeneratedBoolColumn('visto', $tableName, false,
        defaultValue: Constant(false));
  }

  @override
  List<GeneratedColumn> get $columns => [id, name, tipo, nota, recomen, visto];
  @override
  $MoviesTable get asDslTable => this;
  @override
  String get $tableName => _alias ?? 'movies';
  @override
  final String actualTableName = 'movies';
  @override
  VerificationContext validateIntegrity(MoviesCompanion d,
      {bool isInserting = false}) {
    final context = VerificationContext();
    if (d.id.present) {
      context.handle(_idMeta, id.isAcceptableValue(d.id.value, _idMeta));
    } else if (id.isRequired && isInserting) {
      context.missing(_idMeta);
    }
    if (d.name.present) {
      context.handle(
          _nameMeta, name.isAcceptableValue(d.name.value, _nameMeta));
    } else if (name.isRequired && isInserting) {
      context.missing(_nameMeta);
    }
    if (d.tipo.present) {
      context.handle(
          _tipoMeta, tipo.isAcceptableValue(d.tipo.value, _tipoMeta));
    } else if (tipo.isRequired && isInserting) {
      context.missing(_tipoMeta);
    }
    if (d.nota.present) {
      context.handle(
          _notaMeta, nota.isAcceptableValue(d.nota.value, _notaMeta));
    } else if (nota.isRequired && isInserting) {
      context.missing(_notaMeta);
    }
    if (d.recomen.present) {
      context.handle(_recomenMeta,
          recomen.isAcceptableValue(d.recomen.value, _recomenMeta));
    } else if (recomen.isRequired && isInserting) {
      context.missing(_recomenMeta);
    }
    if (d.visto.present) {
      context.handle(
          _vistoMeta, visto.isAcceptableValue(d.visto.value, _vistoMeta));
    } else if (visto.isRequired && isInserting) {
      context.missing(_vistoMeta);
    }
    return context;
  }

  @override
  Set<GeneratedColumn> get $primaryKey => {id};
  @override
  MovieEntry map(Map<String, dynamic> data, {String tablePrefix}) {
    final effectivePrefix = tablePrefix != null ? '$tablePrefix.' : null;
    return MovieEntry.fromData(data, _db, prefix: effectivePrefix);
  }

  @override
  Map<String, Variable> entityToSql(MoviesCompanion d) {
    final map = <String, Variable>{};
    if (d.id.present) {
      map['id'] = Variable<int, IntType>(d.id.value);
    }
    if (d.name.present) {
      map['name'] = Variable<String, StringType>(d.name.value);
    }
    if (d.tipo.present) {
      map['tipo'] = Variable<String, StringType>(d.tipo.value);
    }
    if (d.nota.present) {
      map['nota'] = Variable<String, StringType>(d.nota.value);
    }
    if (d.recomen.present) {
      map['recomen'] = Variable<String, StringType>(d.recomen.value);
    }
    if (d.visto.present) {
      map['visto'] = Variable<bool, BoolType>(d.visto.value);
    }
    return map;
  }

  @override
  $MoviesTable createAlias(String alias) {
    return $MoviesTable(_db, alias);
  }
}

abstract class _$AppDatabase extends GeneratedDatabase {
  _$AppDatabase(QueryExecutor e) : super(SqlTypeSystem.defaultInstance, e);
  $MoviesTable _movies;
  $MoviesTable get movies => _movies ??= $MoviesTable(this);
  MoviesDao _moviesDao;
  MoviesDao get moviesDao => _moviesDao ??= MoviesDao(this as AppDatabase);
  @override
  List<TableInfo> get allTables => [movies];
}
