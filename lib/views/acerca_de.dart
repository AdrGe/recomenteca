import 'package:flutter/material.dart';

class AcercaPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      // backgroundColor: Colors.black,
      appBar: AppBar(
          title: Text('Recomenteca'),
          primary: true,
          automaticallyImplyLeading: true,
          titleSpacing: 4.0),
      body: Padding(
        padding: const EdgeInsets.all(18.0),
        child: Column(children: <Widget>[
          Container(
            decoration: BoxDecoration(
              // color: Colors.blueAccent,
              image: DecorationImage(
                image: AssetImage('rec_about.png'),
                fit: BoxFit.cover,
              ), //DecorationImage
            ),
          ), //BoxDecoration
          Spacer(),
          Text(
            'Icons made by <a href="https://www.flaticon.com/authors/freepik" title="Freepik">Freepik</a>from <a href="https://www.flaticon.com/" title="Flaticon"> www.flaticon.com</a>',
            style: TextStyle(color: Colors.grey),
          )
        ]),
      ),
    );
  }
}
