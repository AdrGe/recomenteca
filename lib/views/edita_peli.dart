import 'package:basesita/data/daos.dart';
import 'package:basesita/data/db.dart';
import 'package:basesita/views/home_page.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class EditaPeli extends StatefulWidget {
  final MovieEntry peli;
  EditaPeli(this.peli);

  @override
  _EditaPeliState createState() => _EditaPeliState();
}

class _EditaPeliState extends State<EditaPeli> {
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  String _name;
  String _tipo;
  String _nota;
  String _recomen;
  bool _visto;

  Widget _buildNomb() {
    return TextFormField(
        decoration: InputDecoration(labelText: 'Nombre'),
        initialValue: widget.peli.name,
        maxLength: 100,
        validator: (String value) {
          if (value.isEmpty) {
            return 'No ingresaste un nombre';
          }
          return null;
        },
        onChanged: (String value) {
          setState(() => _name = value);
        });
  }

  Widget _buildTipo() {
    return Container(
      color: Colors.white,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[
          SizedBox(
            height: 15,
          ),
          Text('Recomendación de',
              style: TextStyle(fontSize: 16, color: Colors.grey[600])),
          Container(
            child: Column(
              children: <Widget>[
                Row(
                  children: <Widget>[
                    Expanded(
                      flex: 1,
                      child: RadioListTile(
                        selected: true,
                        value: 'pelicula',
                        groupValue: _tipo,
                        onChanged: (newValue) =>
                            setState(() => _tipo = newValue),
                        secondary: Text('Film'),
                      ),
                    ),
                    Expanded(
                      flex: 1,
                      child: RadioListTile(
                        value: 'libro',
                        groupValue: _tipo,
                        secondary: Text("Libro"),
                        onChanged: (newValue) =>
                            setState(() => _tipo = newValue),
                        selected: false,
                      ),
                    ),
                    Expanded(
                      flex: 1,
                      child: RadioListTile(
                        value: 'serie',
                        groupValue: _tipo,
                        secondary: Text("Serie"),
                        onChanged: (newValue) =>
                            setState(() => _tipo = newValue),
                        selected: false,
                      ),
                    ),
                    Expanded(
                      flex: 1,
                      child: RadioListTile(
                        value: 'juego',
                        groupValue: _tipo,
                        secondary: Text("Juego"),
                        onChanged: (newValue) =>
                            setState(() => _tipo = newValue),
                        selected: false,
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }

  Widget _buildRecomen() {
    return TextFormField(
      decoration: InputDecoration(labelText: 'Recomendó'),
      initialValue: widget.peli.recomen,
      maxLength: 20,
      onChanged: (String value) {
        setState(() => _recomen = value);
      },
    );
  }

  Widget _buildNota() {
    return TextFormField(
      keyboardType: TextInputType.multiline,
      maxLines: null,
      decoration: InputDecoration(labelText: 'Nota'),
      initialValue: widget.peli.nota,
      // maxLength: 100,
      onChanged: (String value) {
        setState(() => _nota = value);
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    setState(() {
      _visto = widget.peli.visto;
    });
/*     void resetValuesAfterSubmit() {
      setState(() {
        _name = ('');
        _tipo = '';
        _nota = '';
        _recomen = '';
      });
    } */

    void creaMovie() async {
      final _dao = Provider.of<MoviesDao>(context, listen: false);
      MovieEntry nvaPeli = widget.peli.copyWith(
          name: _name,
          tipo: _tipo,
          nota: _nota,
          recomen: _recomen,
          visto: _visto);
      await _dao
          .updateMovie(nvaPeli)
          /* MovieEntry(
              name: _name,
              tipo: _tipo,
              nota: _nota,
              recomen: _recomen,
              visto: _visto)) */
          .then((value) => {
/*                 Scaffold.of(context)
                  ..removeCurrentSnackBar()
                  ..showSnackBar(
                    SnackBar(
                      content: Text('Se guardaron los cambios'),
                      backgroundColor: Colors.purple,
                      duration: Duration(seconds: 3),
                    ),
                  ), */
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => HomePage(),
                  ),
                ),
              })
          .catchError((error) => {
                Navigator.pop(context),
              });
    }

    return Scaffold(
      appBar: AppBar(
        primary: true,
        automaticallyImplyLeading: true,
        titleSpacing: 4.0,
        // title: Text('Recomenteca'),
        bottom: PreferredSize(
          preferredSize: Size.fromHeight(80),
          child: Column(
            children: <Widget>[
              SizedBox(height: 5),
              Padding(
                padding: const EdgeInsets.all(18.0),
                child: Center(
                  child: Row(
                    children: <Widget>[
                      Icon(
                        Icons.feedback,
                        color: Colors.grey[200],
                        size: 26,
                      ),
                      SizedBox(
                        width: 5,
                      ),
                      Center(
                        child: Text(
                          'Editar',
                          style: TextStyle(
                            color: Colors.white,
                            fontSize: 24,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
              SizedBox(height: 5),
            ],
          ),
        ),
      ),
      body: SingleChildScrollView(
        scrollDirection: Axis.vertical,
        child: Container(
          padding: EdgeInsets.all(10.0),
          // color: Colors.white,
          child: Form(
            autovalidate: true,
            key: _formKey,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                _buildTipo(),
                _buildNomb(),
                _buildRecomen(),
                _buildNota(),
              ],
            ),
          ),
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: creaMovie,
        tooltip: 'Guardar',
        child: Icon(Icons.check),
        backgroundColor: Colors.lightBlue[200],
        foregroundColor: Colors.indigo[900],
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.endTop,
    );
  }
}
