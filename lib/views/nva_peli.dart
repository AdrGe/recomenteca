import 'package:basesita/data/daos.dart';
import 'package:basesita/data/db.dart';
import 'package:flutter/material.dart';
import 'package:moor/moor.dart' hide Column;
// import 'package:moor_flutter/moor_flutter.dart' hide Column;
import 'package:provider/provider.dart';

class NuevaPeli extends StatefulWidget {
  @override
  _NuevaPeliState createState() => _NuevaPeliState();
}

class _NuevaPeliState extends State<NuevaPeli> {
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  String _name;
  String _tipo;
  String _nota;
  String _recomen;

  Widget _buildNomb() {
    return TextFormField(
        textCapitalization: TextCapitalization.sentences,
        decoration: InputDecoration(labelText: 'Nombre'),
        maxLength: 100,
        validator: (String value) {
          if (value.isEmpty) {
            return 'No ingresaste un nombre';
          }
          return null;
        },
        onChanged: (String value) {
          setState(() => _name = value);
        });
  }

  Widget _buildTipo() {
    return Container(
      color: Colors.white,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[
          SizedBox(
            height: 15,
          ),
          Text('Recomendación de',
              style: TextStyle(fontSize: 16, color: Colors.grey[600])),
          Container(
            child: Column(
              children: <Widget>[
                Row(
                  children: <Widget>[
                    Expanded(
                      // flex: 1,
                      child: RadioListTile(
                        selected: true,
                        value: 'pelicula',
                        groupValue: _tipo,
                        onChanged: (newValue) =>
                            setState(() => _tipo = newValue),
                        secondary: Text('Película'),
                      ),
                    ),
                    Expanded(
                      // flex: 1,
                      child: RadioListTile(
                        value: 'libro',
                        groupValue: _tipo,
                        secondary: Text("Libro"),
                        onChanged: (newValue) =>
                            setState(() => _tipo = newValue),
                        selected: false,
                      ),
                    ),
                    Expanded(
                      // flex: 1,
                      child: RadioListTile(
                        value: 'serie',
                        groupValue: _tipo,
                        secondary: Text("Serie"),
                        onChanged: (newValue) =>
                            setState(() => _tipo = newValue),
                        selected: false,
                      ),
                    ),
                    Expanded(
                      // flex: 1,
                      child: RadioListTile(
                        value: 'juego',
                        groupValue: _tipo,
                        secondary: Text("Juego"),
                        onChanged: (newValue) =>
                            setState(() => _tipo = newValue),
                        selected: false,
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }

  Widget _buildRecomen() {
    return TextFormField(
      textCapitalization: TextCapitalization.sentences,
      decoration: InputDecoration(labelText: 'Recomendó'),
      maxLength: 20,
      onChanged: (String value) {
        setState(() => _recomen = value);
      },
    );
  }

  Widget _buildNota() {
    return TextFormField(
      keyboardType: TextInputType.multiline,
      maxLines: null,
      textCapitalization: TextCapitalization.sentences,
      decoration: InputDecoration(labelText: 'Nota'),
      onChanged: (String value) {
        setState(() => _nota = value);
      },
    );
  }

  @override
  Widget build(BuildContext context) {
/*     void resetValuesAfterSubmit() {
      setState(() {
        _name = ('');
        _tipo = '';
        _nota = '';
        _recomen = '';
      });
    } */

    void creaMovie() async {
      final _dao = Provider.of<MoviesDao>(context, listen: false);
      await _dao
          .insertMovie(MoviesCompanion(
              name: Value(_name),
              tipo: Value(_tipo),
              nota: Value(_nota),
              recomen: Value(_recomen)))
          .then((value) => {
                Navigator.pop(context, 'Se guardó con éxito'),
              })
          .catchError((error) => {
                Navigator.pop(
                    context, 'No se pudo guardar. ¿Hay espacio en disco?'),
              });
    }

    return Scaffold(
      appBar: AppBar(
        primary: true,
        automaticallyImplyLeading: true,
        titleSpacing: 4.0,
        // title: Text('Recomenteca'),
        bottom: PreferredSize(
          preferredSize: Size.fromHeight(80),
          child: Column(
            children: <Widget>[
              SizedBox(height: 5),
              Row(
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.only(left: 14),
                    child: Text(
                      'Nueva Recomendación',
                      style: TextStyle(
                        color: Colors.white,
                        fontSize: 24,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                  ),
                  SizedBox(
                    width: 10,
                  ),
                  Icon(
                    Icons.feedback,
                    size: 24,
                    color: Colors.white,
                  ),
                ],
              ),
              SizedBox(height: 35),
            ],
          ),
        ),
      ),
      body: SingleChildScrollView(
        scrollDirection: Axis.vertical,
        child: Container(
          padding: EdgeInsets.all(10.0),
          // color: Colors.white,
          child: Form(
            autovalidate: true,
            key: _formKey,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                _buildTipo(),
                _buildNomb(),
                _buildRecomen(),
                _buildNota(),
                SizedBox(
                  height: 10,
                ),
              ],
            ),
          ),
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: creaMovie,
        tooltip: 'Guardar',
        child: Icon(Icons.check),
        backgroundColor: Colors.lightBlue[200],
        foregroundColor: Colors.indigo[900],
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.endTop,
    );
  }
}
