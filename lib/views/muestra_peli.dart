import 'package:basesita/data/db.dart';
import 'package:flutter/material.dart';

import 'edita_peli.dart';

class MuestraPeli extends StatefulWidget {
  final MovieEntry peli;
  MuestraPeli(this.peli);

  @override
  _MuestraPeliState createState() => _MuestraPeliState();
}

class _MuestraPeliState extends State<MuestraPeli> {
  @override
  Widget build(BuildContext context) {
    final double _textSize = 22;

    return Scaffold(
      appBar: AppBar(
        primary: true,
        automaticallyImplyLeading: true,
        titleSpacing: 4.0,
        bottom: PreferredSize(
          preferredSize: Size.fromHeight(80),
          child: Column(
            children: <Widget>[
              SizedBox(height: 5),
              Padding(
                padding: const EdgeInsets.all(18.0),
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Icon(
                      Icons.stars,
                      color: Colors.grey[200],
                      size: 26,
                    ),
                    SizedBox(width: 12),
                    Flexible(
                      child: Text(
                        widget.peli.name.toString(),
                        style: TextStyle(
                          fontWeight: FontWeight.bold,
                          color: Colors.white,
                          fontSize: 26,
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              SizedBox(height: 5)
            ],
          ),
        ),
      ),
      body: GestureDetector(
        onHorizontalDragUpdate: (details) {
          print(details.primaryDelta);
          if (details.primaryDelta < 0) {
            Navigator.of(context).pop();
            print('gesto');
          }
        },
        child: Padding(
          padding: const EdgeInsets.all(18.0),
          child: Column(
            children: <Widget>[
              SizedBox(height: 20),
              Row(
                children: <Widget>[
                  _buildIcon(widget.peli),
                  SizedBox(
                    width: 5,
                  ),
                  Text(
                    'Tipo: ',
                    style: TextStyle(
                        fontSize: _textSize,
                        fontWeight: FontWeight.bold,
                        color: Colors.grey[700]),
                  ),
                  Text(
                    '${widget.peli.tipo}',
                    style: TextStyle(
                      fontSize: _textSize,
                    ),
                  ),
                ],
              ),
              SizedBox(height: 22),
              Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Icon(
                    Icons.person,
                    color: Colors.grey[600],
                  ),
                  SizedBox(
                    width: 5,
                  ),
                  Text(
                    'Recomendó: ',
                    style: TextStyle(
                        fontSize: _textSize,
                        fontWeight: FontWeight.bold,
                        color: Colors.grey[700]),
                  ),
                  Flexible(
                    child: Text(
                      '${widget.peli.recomen}',
                      style: TextStyle(
                        fontSize: _textSize,
                      ),
                    ),
                  ),
                ],
              ),
              SizedBox(height: 22),
              Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Icon(
                    Icons.note_add,
                    color: Colors.grey[600],
                  ),
                  SizedBox(
                    width: 5,
                  ),
                  Text(
                    'Notas: ',
                    style: TextStyle(
                        fontSize: _textSize,
                        fontWeight: FontWeight.bold,
                        color: Colors.grey[700]),
                  ),
                  Flexible(
                    child: Text(
                      '${widget.peli.nota}',
                      style: TextStyle(
                        fontSize: _textSize,
                      ),
                    ),
                  ),
                ],
              ),
              SizedBox(height: 22),
              _buildVisto(widget.peli),
              // Spacer(flex: 2),
            ],
          ),
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          Navigator.of(context).push(
              MaterialPageRoute(builder: (context) => EditaPeli(widget.peli)));
        },
        tooltip: 'Guardar',
        child: Icon(Icons.edit),
        backgroundColor: Colors.lightBlue[200],
        foregroundColor: Colors.indigo[900],
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.endTop,
    );
  }

  Row _buildVisto(MovieEntry itemMovie) {
    if (widget.peli.visto) {
      return Row(children: <Widget>[
        Icon(
          Icons.remove_red_eye,
          color: Colors.grey[600],
        ),
        SizedBox(
          width: 5,
        ),
        Text('Visto: ',
            style: TextStyle(
                fontSize: 22,
                fontWeight: FontWeight.bold,
                color: Colors.grey[700])),
        Text('SI', style: TextStyle(fontSize: 22)),
      ]);
    } else {
      return Row(
        children: <Widget>[
          Icon(
            Icons.visibility_off,
            color: Colors.grey[700],
          ),
          SizedBox(
            width: 5,
          ),
          Text('Visto: ',
              style: TextStyle(
                  fontSize: 22,
                  fontWeight: FontWeight.bold,
                  color: Colors.grey[700])),
          Text('NO', style: TextStyle(fontSize: 22))
        ],
      );
    }
  }

  Widget _buildIcon(MovieEntry itemMovie) {
    switch (itemMovie.tipo) {
      case 'pelicula':
        return Icon(
          Icons.movie,
          color: Colors.grey[600],
        );
        break;
      case 'serie':
        return Icon(
          Icons.live_tv,
          color: Colors.grey[600],
        );
        break;
      case 'libro':
        return Icon(
          Icons.book,
          color: Colors.grey[600],
        );
        break;
      case 'juego':
        return Icon(
          Icons.games,
          color: Colors.grey[600],
        );
        break;
      default:
        return Icon(
          Icons.compare_arrows,
          color: Colors.grey[600],
        );
        break;
    }
  }
}
