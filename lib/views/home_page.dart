import 'package:basesita/data/daos.dart';
import 'package:basesita/views/acerca_de.dart';
import 'package:basesita/views/muestra_peli.dart';
import 'package:basesita/views/nva_peli.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:flutter_slidable/flutter_slidable.dart';
import 'package:share/share.dart';
import 'package:basesita/data/db.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  bool _muestraVistos = false;
  bool _visto = false;
  // bool _buscar = false;
  String _filt = '';
  bool _todo = true;
  double _width;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
// TODO:implementar SearchAppbar desde: https://github.com/rodolfoggp/search_app_bar/tree/master/example
        title: Text('Recomenteca'),
        automaticallyImplyLeading: true,
        actions: <Widget>[
          IconButton(
            icon: Icon(Icons.help_outline),
            onPressed: () {
              Navigator.of(context)
                  .push(MaterialPageRoute(builder: (context) => AcercaPage()));
            },
          )
        ],
        bottom: PreferredSize(
          preferredSize: Size.fromHeight(60),
          child: Column(
            children: <Widget>[
              SizedBox(height: 5),
              Center(
                child: Row(
                  children: <Widget>[
                    _todo
                        ? FlatButton.icon(
                            icon: Icon(Icons.filter_list),
                            label: Text(
                              'Rubro',
                              textAlign: TextAlign.right,
                              style: TextStyle(
                                  color: Colors.grey[100],
                                  fontWeight: FontWeight.bold),
                            ),
                            onPressed: () {
                              setState(() {
                                _filt = '';
                                _todo = false;
                              });
                            },
                          )
                        : AnimatedContainer(
                            duration: Duration(milliseconds: 1600),
                            curve: Curves.bounceIn,
                            width: _width,
                            child: Row(
                              children: <Widget>[
                                FlatButton.icon(
                                  icon: Icon(Icons.bubble_chart),
                                  label: Text(
                                    'Todos',
                                    textAlign: TextAlign.right,
                                    style: TextStyle(
                                        color: Colors.grey[100],
                                        fontWeight: FontWeight.bold),
                                  ),
                                  onPressed: () {
                                    setState(() {
                                      _filt = '';
                                      _todo = true;
                                      // _width = 120.0;
                                    });
                                  },
                                ),
                                IconButton(
                                    icon: Icon(
                                      Icons.movie,
                                      color: Colors.white,
                                      size: 24,
                                    ),
                                    onPressed: () {
                                      setState(() {
                                        _filt = 'pelicula';
                                      });
                                    }),
                                IconButton(
                                    icon: Icon(
                                      Icons.live_tv,
                                      color: Colors.white,
                                      size: 24,
                                    ),
                                    onPressed: () {
                                      setState(() {
                                        _filt = 'serie';
                                      });
                                    }),
                                IconButton(
                                    icon: Icon(
                                      Icons.book,
                                      color: Colors.white,
                                      size: 24,
                                    ),
                                    onPressed: () {
                                      setState(() {
                                        _filt = 'libro';
                                      });
                                    }),
                                IconButton(
                                    icon: Icon(
                                      Icons.games,
                                      color: Colors.white,
                                      size: 24,
                                    ),
                                    onPressed: () {
                                      setState(() {
                                        _filt = 'juego';
                                      });
                                    }),
                              ],
                            ),
                          ),
                    Spacer(),
                    _buildMuestraVistosSwitch(),
                  ],
                ),
              ),
              SizedBox(
                height: 5,
              )
            ],
          ),
        ),
      ),
      body: Column(
        children: <Widget>[
          Expanded(child: _buildList(context)),
          // NuevaPeli(),
        ],
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          Navigator.of(context)
              .push(MaterialPageRoute(builder: (context) => NuevaPeli()));
        },
        tooltip: 'Nuevo',
        child: Icon(Icons.add),
      ),
    );
  }

  void _compartePeli(MovieEntry peli) {
    Share.share('Te comparto: ${peli.tipo} : ${peli.name}, ${peli.nota}',
        subject: 'Recomiendo');
  }

  void _cVisto(MovieEntry peli) async {
    setState(() {
      _visto = !_visto;
    });
    MovieEntry nvaPeli = peli.copyWith(visto: _visto);
    await Provider.of<MoviesDao>(context, listen: false).updateMovie(nvaPeli);
  }
/* void _cBusca() {
    setState(() {
       if (field == "tag"){
        visibilityTag = visibility;
      }
      if (field == "obs"){
        visibilityObs = visibility;
      } 
      _buscar = !_buscar;
    });
  } */

  Row _buildMuestraVistosSwitch() {
    return Row(
      children: <Widget>[
        Text('Visto'),
        Switch(
          value: _muestraVistos,
          activeColor: Colors.white,
          onChanged: (newValue) {
            setState(() {
              _muestraVistos = newValue;
            });
          },
        ),
      ],
    );
  }

  StreamBuilder<List<MovieEntry>> _buildList(BuildContext context) {
    final dao = Provider.of<MoviesDao>(context);
    return StreamBuilder(
      stream: _muestraVistos ? dao.watchAll(_filt) : dao.watchPendiente(_filt),
      builder: (context, AsyncSnapshot<List<MovieEntry>> snapshot) {
        final pelis = snapshot.data ?? List();
        return ListView.builder(
          itemCount: pelis.length,
          itemBuilder: (_, index) {
            final itemMovie = pelis[index];
            return _buildListItem(itemMovie, dao);
          },
        );
      },
    );
  }

  Widget _buildIcon(MovieEntry itemMovie) {
    switch (itemMovie.tipo) {
      case 'pelicula':
        return Icon(Icons.movie);
        break;
      case 'serie':
        return Icon(Icons.live_tv);
        break;
      case 'libro':
        return Icon(Icons.book);
        break;
      case 'juego':
        return Icon(Icons.games);
        break;
      default:
        return Icon(Icons.compare_arrows);
        break;
    }
  }

  Widget _buildListItem(MovieEntry itemMovie, MoviesDao dao) {
    return Slidable(
      actionPane: SlidableDrawerActionPane(),
      actions: <Widget>[
        IconSlideAction(
          caption: 'Compartir',
          icon: Icons.share,
          color: Colors.green,
          onTap: () => _compartePeli(itemMovie),
        ),
        IconSlideAction(
          caption: 'Visto',
          color: Colors.purple,
          icon: itemMovie.visto ? Icons.visibility : Icons.visibility_off,
          onTap: () => this._cVisto(itemMovie),
        ),
      ],
      secondaryActions: <Widget>[
        IconSlideAction(
          caption: 'Borrar',
          color: Colors.red,
          icon: Icons.delete,
          onTap: () => dao.deleteMovie(itemMovie),
        ),
      ],
      child: Column(
        children: <Widget>[
          Container(
            // color: Colors.white,
            child: InkWell(
              onLongPress: () {
                Navigator.of(context).push(
                  MaterialPageRoute(
                    builder: (context) => MuestraPeli(itemMovie),
                  ),
                );
              },
              enableFeedback: true,
              splashColor: Colors.lightBlue[200],
              child: ListTile(
                leading: _buildIcon(itemMovie),
                title: Text(
                  itemMovie.name,
                  overflow: TextOverflow.fade,
                  style: itemMovie.visto
                      ? TextStyle(
                          color: Colors.grey, fontWeight: FontWeight.bold)
                      : TextStyle(
                          color: Colors.black, fontWeight: FontWeight.bold),
                ),
                subtitle: Text(
                  'Recomendó: ${itemMovie.recomen}. ${itemMovie.nota}',
                  overflow: TextOverflow.fade,
                  style: itemMovie.visto
                      ? TextStyle(color: Colors.grey)
                      : TextStyle(color: Colors.black),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
