import 'package:basesita/data/db.dart';
// import 'package:basesita/nva_peli.dart';
// import 'package:basesita/pelis.dart';
// import 'package:basesita/series.dart';
// import 'package:basesita/juegos.dart';
import 'package:basesita/views/home_page.dart';

import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
// import 'home_page.dart';
// import 'libros.dart';

void main() {
  Provider.debugCheckInvalidValueType = null;
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Provider(
      create: (_) => AppDatabase().moviesDao,
      child: GestureDetector(
        onTap: () {
          FocusScopeNode currentFocus = FocusScope.of(context);
          if (!currentFocus.hasPrimaryFocus) {
            currentFocus.unfocus();
          }
        },
        child: MaterialApp(
          title: 'Recomenteca',
          home:
              HomePage(), /*  DefaultTabController(
            length: 4,
            child: Scaffold(
              appBar: AppBar(
                title: Text('Recomenteca'),
                bottom: TabBar(
                  tabs: [
                    Tab(
                      icon: Icon(Icons.live_tv),
                      text: 'Series',
                    ),
                    Tab(
                      icon: Icon(Icons.movie),
                      text: 'Películas',
                    ),
                    Tab(
                      icon: Icon(Icons.book),
                      text: 'Libros',
                    ),
                    Tab(
                      icon: Icon(Icons.gamepad),
                      text: 'Juegos',
                    )
                  ],
                ),
              ),
              body: TabBarView(
                children: [
                  SeriesPage(),
                  PeliculasPage(),
                  LibrosPage(),
                  JuegosPage(),
                ],
              ),
            ),
          ), */
          // routes: <String, WidgetBuilder>{
          //   '/nvaPeli': (context) => NuevaPeli(),
          // },
        ),
      ),
    );
  }
}
