# Recomenteca

Una aplicación para anotar y manejar las recomendaciones que nos hacen sobre cine, series, libros y juegos.
Podremos recopilar las recomendaciones que nos hacen amigos, las que escuchamos por radio, las sugerencias de blogs o diarios, y compartirlas desde nuestro teléfono.

Desarrollada en [Flutter](https://flutter.dev/), hace uso de una base de datos local a la aplicación(Sqlite) gracias a la librería [Moor](https://moor.simonbinder.eu/).
Disfruten y compartan.

Application to manage recomendations that we collect about movies, TVShows, books and games. With recomenteca we can collect those recomendations from friends, from radio shows, from blog posts or news paper articles, and share it from our phone.

Recomenteca was develop with [Flutter](https://flutter.dev/), using a local database (Sqlite) as a backend thanks to [Moor library](https://moor.simonbinder.eu/) to persist the data into de phones.
Enjoy and share it.
